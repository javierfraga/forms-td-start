import { Component, ViewChild,OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
	@ViewChild('f') signupForm:NgForm;
	@ViewChild('radios') radios:NgForm;
	@ViewChild('email') email:NgForm;
	private defaultQuestion:string = 'pet';
	private genders:Array<string>= ['male','female'];
	private submitted:boolean = false;
	private answers:any = {
		// username: '',
		// email: '',
		// secret: '',
		// answer: '',
		// gender: '',
		// submitted: '',

	};

  ngOnInit() {
    /*
     * Very cool console logs anytime there is a value change in form
     * this is overkill on console so uncomment to test if you want
     */
    this.signupForm.valueChanges.subscribe(
      (value)=>console.log(value)
    );
    /*
     * Very cool console logs anytime there is a value change and reports new status
     * this is overkill on console so uncomment to test if you want
     */
    this.signupForm.statusChanges.subscribe(
      (status)=>console.log(status)
    );  
  }

  suggestUserName() {
    const suggestedName = 'Superuser';
    /*
     * setValue requires the entire form object
     * must be entire form object
     */
    this.signupForm.setValue({
    	userData: {
    		username: suggestedName,
    		email: 'javier.s.fraga@gmail.com'
    	},
    	secret: 'pet',
    	questionAnswer: 'santos',
    	gender: 'male'
    })
    /*
     * sets any number of form elements
     */
    // this.signupForm.form.patchValue({
    // 	userData: {
    // 		username: suggestedName
    // 	},
    // 	gender: 'female'
    // })
  }

  // onSubmit(form:NgForm) {
  // 	console.log(form)
  // }
  // Useful if you need this early than when you submit form
  onSubmit() {
  	console.log(this.signupForm);
  	console.log(this.email);
  	console.log(this.radios);
  	this.answers.username = this.signupForm.value.userData.username;
  	this.answers.email = this.signupForm.value.userData.email;
  	this.answers.secret = this.signupForm.value.secret;
  	this.answers.answer = this.signupForm.value.questionAnswer;
  	this.answers.gender = this.signupForm.value.gender;
  	this.submitted = true;

  	// can even reset to certain values like the setValue but I guess has to be entire form
  	this.signupForm.reset();
  }
}
